# Clone Hero chart reverser

## Instructions

1. Install python from https://www.python.org/downloads/
2. Download the `reverse.py` script.
3. Open the terminal in the directory with the script.
4. Run the command:
```
python3 reverse.py <path_to_chart_file>
```
(replace <path_to_chart_file> with the path to the .chart file you want to reverse)

5. The script should create a `.reversed.chart` file in the directory of the chart file. You can rename it to `notes.chart` to use it.

## Notes

- The audio won't be reversed, you will have to use another program like Audacity to do that. https://www.audacityteam.org/download/
- Some sustains will start off-beat, due to a charting convention; sustains are usually cut-off about a sixteenth note before the next note.
- Lyrics mostly work, however, not all charts have a `phrase_end` event at the end of the lyric phrase. Phrases without the `phrase_end` (which get converted to `phrase_start` by the script) will not show up in-game.
- Some songs will have misaligned time signatures after reversing. You can open the reversed chart in Moonscraper and hit Ctrl+S to check where the misaligned time signatures are and fix them.
