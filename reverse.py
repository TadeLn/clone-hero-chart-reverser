import sys

difficulties = [
    "Expert",
    "Hard",
    "Medium",
    "Easy"
]

instruments = [
    "Single",
    "DoubleGuitar",
    "DoubleBass",
    "DoubleRhythm",
    "Keyboard",
    "Drums",
    "GHLGuitar",
    "GHLBass"
]

chart_types = []

for instrument in instruments:
    for difficulty in difficulties:
        chart_types.append(f"{difficulty}{instrument}")

def get_event_type(event_data):
    pure_event_data = event_data.strip()
    if pure_event_data[0] == '"' and pure_event_data[-1] == '"':
        pure_event_data = pure_event_data[1:-1]

    return pure_event_data.split(" ")[0]



def load_sections_from_chart_format(chart_file_text):
    sections = {}
    lines = chart_file_text.split("\n")
    current_section_content = {}
    current_section_name = "Song"
    for line in lines:
        line_stripped = line.strip()

        if line_stripped == "":
            continue

        if line_stripped.startswith("[") and line_stripped.endswith("]"):
            current_section_name = line_stripped[1:-1]
            print(f"Reading section: {current_section_name}")

        elif line_stripped == "{":
            current_section_content = {}

        elif line_stripped == "}":
            sections[current_section_name] = current_section_content

        else:
            segments = line_stripped.split("=")
            key = segments[0].strip()
            value = "=".join(segments[1:]).strip()

            if key in dict.keys(current_section_content):
                current_section_content[key].append(value)
            else:
                current_section_content[key] = [value]

    return sections


def load_global_section(sections, section_name, last_tick):
    chart_section = sections[section_name]
    chart_events = []

    for tick in dict.keys(chart_section):
        for event in chart_section[tick]:
            tick_num = int(tick)
            if tick_num > last_tick:
                last_tick = tick_num

            event_segments = event.split(" ")
            event_type = event_segments[0]

            if event_type == "B":
                chart_events.append({
                    "tick": tick_num,
                    "type": "bpm",
                    "value": int(event_segments[1])
                })
            elif event_type == "TS":
                chart_events.append({
                    "tick": tick_num,
                    "type": "time_signature",
                    "value": int(event_segments[1])
                })
            elif event_type == "E":
                event_data = " ".join(event_segments[1:])
                chart_events.append({
                    "tick": tick_num,
                    "type": "event",
                    "event": event_data,
                    "event_type": get_event_type(event_data)
                })
            else:
                print(f"ERROR: unknown event type: {event_type} @ tick {tick_num} ({section_name})")

    return (chart_events, last_tick)


def load_chart_from_section(chart_section, chart_key, last_tick):
    chart_notes = []

    for tick in dict.keys(chart_section):
        tick_num = int(tick)
        is_forced = False
        is_tap = False
        is_yellow_cymbal = False
        is_blue_cymbal = False
        is_green_cymbal = False
        notes_to_add = []

        for note in chart_section[tick]:
            note_segments = note.split(" ")
            note_type = note_segments[0]

            if note_type == "N":
                fret = int(note_segments[1])
                duration = int(note_segments[2])

                if tick_num + duration > last_tick:
                    last_tick = tick_num + duration

                if fret == 5:
                    is_forced = True
                if fret == 6:
                    is_tap = True
                elif fret == 66:
                    is_yellow_cymbal = True
                elif fret == 67:
                    is_blue_cymbal = True
                elif fret == 68:
                    is_green_cymbal = True
                else:
                    notes_to_add.append({
                        "fret": fret,
                        "duration": duration
                    })

            elif note_type == "S":
                fret = int(note_segments[1])
                duration = int(note_segments[2])

                if tick_num + duration > last_tick:
                    last_tick = tick_num + duration

                chart_notes.append({
                    "tick": tick_num,
                    "type": "starpower",
                    "fret": fret,
                    "duration": duration
                })

            elif note_type == "E":
                if tick_num > last_tick:
                    last_tick = tick_num

                event_data = " ".join(note_segments[1:])
                chart_notes.append({
                    "tick": tick_num,
                    "type": "event",
                    "event": " ".join(event_data),
                    "event_type": get_event_type(event_data)
                })

            else:
                print(f"ERROR: unknown note type: {note_segments[0]} @ tick {tick} ({chart_key})")

        for note in notes_to_add:
            chart_notes.append({
                "tick": int(tick),
                "type": "note",
                "fret": note["fret"],
                "duration": note["duration"],
                "is_forced": is_forced,
                "is_tap": is_tap,
                "is_yellow_cymbal": is_yellow_cymbal,
                "is_blue_cymbal": is_blue_cymbal,
                "is_green_cymbal": is_green_cymbal
            })

    return (chart_notes, last_tick)



def load_song_from_chart_format(chart_file_text):
    sections = load_sections_from_chart_format(chart_file_text)
    last_tick = 0

    song = {
        "song": {},
        "sync_track": [],
        "events": [],
        "charts": {},
        "last_tick": 0
    }

    for key in dict.keys(sections["Song"]):
        song["song"][key] = sections["Song"][key][0]
    
    (song["sync_track"], last_tick) = load_global_section(sections, "SyncTrack", last_tick)
    (song["events"], last_tick) = load_global_section(sections, "Events", last_tick)
    
    for chart_key in chart_types:
        if chart_key in dict.keys(sections):
            (song["charts"][chart_key], last_tick) = load_chart_from_section(sections[chart_key], chart_key, last_tick)

    song["last_tick"] = last_tick

    return song



def section_to_string(section_name, key_values):
    output = f"[{section_name}]\n"
    output += "{\n"
    for key, values in key_values.items():
        for value in values:
            output += f"  {key} = {value}\n"
    output += "}\n"
    return output



def sort_by_tick_callback(item):
    return item["tick"]

def song_events_to_section(events, section_key):
    events.sort(key=sort_by_tick_callback)
    events_data = {}
    for event in events:
        tick = str(event["tick"])
        event_str = ""

        event_type = event["type"]
        event_types = {
            "bpm": "B",
            "time_signature": "TS",
            "event": "E"
        }
        event_type_letter = event_types[event_type]

        if event_type == "bpm" or event_type == "time_signature":
            value = event["value"]
            event_str = f"{event_type_letter} {value}"
        elif event_type == "event":
            event_ = event["event"]
            event_str = f"{event_type_letter} {event_}"
        else:
            print(f"ERROR: unknown event type: {event_type} @ tick {tick} ({section_key})")
        

        if tick in events_data:
            events_data[tick].append(event_str)
        else:
            events_data[tick] = [event_str]

    return events_data

def chart_notes_to_section(chart_key, notes):
    notes.sort(key=sort_by_tick_callback)
    notes_data = {}
    for note in notes:
        tick = str(note["tick"])
        note_str = ""

        note_type = note["type"]
        note_types = {
            "note": "N",
            "starpower": "S",
            "event": "E"
        }
        note_type_letter = note_types[note_type]

        if note_type == "note" or note_type == "starpower":
            fret = note["fret"]
            duration = note["duration"]
            note_str = f"{note_type_letter} {fret} {duration}"
        elif note_type == "event":
            event = note["event"]
            note_str = f"{note_type_letter} {event}"
        else:
            print(f"ERROR: unknown note type: {note_type} @ tick {tick} ({chart_key})")

        if tick in notes_data:
            notes_data[tick].append(note_str)
        else:
            notes_data[tick] = [note_str]

        if note_type == "note":
            if note["is_forced"]:
                notes_data[tick].append(f"N 5 0")
            if note["is_tap"]:
                notes_data[tick].append(f"N 6 0")
            if note["is_yellow_cymbal"]:
                notes_data[tick].append(f"N 66 0")
            if note["is_blue_cymbal"]:
                notes_data[tick].append(f"N 67 0")
            if note["is_green_cymbal"]:
                notes_data[tick].append(f"N 68 0")

    return notes_data

def save_song_to_chart_format(song):
    output_str = ""

    song_data = {}
    for key, value in song["song"].items():
        song_data[key] = [value]

    output_str += section_to_string("Song", song_data)
    output_str += section_to_string("SyncTrack", song_events_to_section(song["sync_track"], "sync_track"))
    output_str += section_to_string("Events", song_events_to_section(song["events"], "events"))
    
    for chart_key, chart in song["charts"].items():
        output_str += section_to_string(chart_key, chart_notes_to_section(chart_key, chart))

    return output_str



def load_chart_from_file(filename):
    with open(filename, "r") as f:
        return load_song_from_chart_format(f.read())
    
def save_chart_to_file(filename, chart_data):
    with open(filename, "w") as f:
        return f.write(save_song_to_chart_format(chart_data))
    


def reverse_sections(sections, last_tick):
    if len(sections) == 0:
        return
    
    for i in range(len(sections) - 1):
        current_section = sections[i]
        current_section_end = sections[i + 1]["tick"]
        current_section["tick"] = last_tick - current_section_end

    # Last section will always be at the very beginning of the reversed chart
    sections[-1]["tick"] = 0

def reverse(song):
    last_tick = song["last_tick"]

    time_signatures = []
    tempos = []
    sections = []
    events = []

    for event in song["sync_track"]:
        if event["type"] == "time_signature":
            time_signatures.append(event)

        elif event["type"] == "bpm":
            tempos.append(event)

    for event in song["events"]:
        if event["type"] == "event":
            if "event_type" in event and event["event_type"] == "section":
                sections.append(event)
            else:
                events.append(event)

    reverse_sections(time_signatures, last_tick)
    reverse_sections(tempos, last_tick)
    reverse_sections(sections, last_tick)

    swap_dict = {
        "solo": "solo_end",
        "solo_end": "solo",
        "phrase_start": "phrase_end",
        "phrase_end": "phrase_start"
    }

    for event in events:
        if event["event_type"] in swap_dict:
            event["event"] = swap_dict[event["event_type"]]
            event["event_type"] = get_event_type(event["event"])

        tick = event["tick"]
        event["tick"] = last_tick - tick


    sections.sort(key=sort_by_tick_callback)
    for section in sections[1:]:
        section["tick"] += 1


    for chart in song["charts"].values():
        for note in chart:
            tick = note["tick"]

            if note["type"] == "event":
                if note["event_type"] in swap_dict:
                    note["event"] = swap_dict[note["event_type"]]
                    note["event_type"] = get_event_type(note["event"])

            if "duration" in note:
                duration = note["duration"]
                note["tick"] = last_tick - (tick + duration)
            else:
                note["tick"] = last_tick - tick

    inner_song_name = song["song"]["Name"][1:-1]
    song["song"]["Name"] = f"\"{inner_song_name} (Reversed)\""
    


def main():
    if len(sys.argv) > 1:
        input_filename = sys.argv[1]
        print(f"Converting {input_filename}...")
        song = load_chart_from_file(input_filename)

        reverse(song)

        output_filename = ".".join([*input_filename.split(".")[:-1], "reversed", "chart"])
        save_chart_to_file(output_filename, song)
    else:
        print(f"Usage: python3 {sys.argv[0]} <chart_file>")

main()
